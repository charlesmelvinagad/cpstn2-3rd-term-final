﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorDisplay : MonoBehaviour {

    public bool enableCursor;
	
	// Update is called once per frame
	void Update () {

        //Cursor.lockState = CursorLockMode.Locked;
        // Hide the cursor
        Cursor.visible = enableCursor;
    }
}
