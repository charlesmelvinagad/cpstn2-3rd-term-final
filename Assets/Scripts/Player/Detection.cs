﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Detection : MonoBehaviour {

    public CameraController cam;
    public CursorDisplay cursorDisplay;
    public Text instruction;
    public int rayLength;
    
	// Use this for initialization
	void Start () {

    
    }
	
	// Update is called once per frame
	void Update () {

        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, rayLength))
        {
            DetectPuzzle(hit);
            InteractItems(hit);
        }

        else
        {
            instruction.text = "";
        }

            
    }

    void DetectPuzzle(RaycastHit _hit)
    {
        if (_hit.collider.tag == "Puzzle")
        {

        instruction.text = "[E] to interact";

            if (Input.GetKeyDown(KeyCode.E))
                {
                    cursorDisplay.enableCursor = true;
                    cam.cam[1].enabled = true;
                }
        }
    }

    void InteractItems(RaycastHit _hit)
    {
        if (_hit.collider.tag == "InteractItem")
        {

            if (Input.GetKeyDown(KeyCode.E))
            {
                Destroy(_hit.collider.gameObject);
                _hit.collider.GetComponent<TextAssetHolder>().SendText();
            }

            Debug.Log("working");
            _hit.collider.GetComponent<GlowObject>().GlowOn();
        }
    }

    void GlowOff(RaycastHit _hit)
    {
       
    }


}
