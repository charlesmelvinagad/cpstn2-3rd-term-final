﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitPuzzle : MonoBehaviour {

    public ST_PuzzleDisplay puzzleDisplay;
    public CursorDisplay cursorDisplay;
    public CameraController cam;

    // Update is called once per frame
    void Update () {

        PuzzleComplete();
        Exit();

    }


    void PuzzleComplete()
    {
        if (puzzleDisplay.Complete)
        {
            cursorDisplay.enableCursor = false;
            cam.cam[1].enabled = false;
        }
    }

    void Exit()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            cursorDisplay.enableCursor = false;
            cam.cam[1].enabled = false;
        }
    }
}
