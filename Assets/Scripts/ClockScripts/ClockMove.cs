﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


public class ClockMove : MonoBehaviour {


	private const float
	hoursToDegrees = 360f / 12f,
	minutesToDegrees = 360f / 60f,
	secondsToDegrees = 360f / 60f;

	public Transform hours, minutes/*, seconds*/;
    //public bool analog;

    //Rigidbody rb;

	public float speed;

	// Use this for initialization
	void Start () {
       // rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {

        /*
		if (analog) {
			TimeSpan timespan = DateTime.Now.TimeOfDay;
			hours.localRotation =
				Quaternion.Euler(0f,0f,(float)timespan.TotalHours * -hoursToDegrees);
			minutes.localRotation =
				Quaternion.Euler(0f,0f,(float)timespan.TotalMinutes * -minutesToDegrees);
			seconds.localRotation =
				Quaternion.Euler(0f,0f,(float)timespan.TotalSeconds * -secondsToDegrees);
		}
		
		    DateTime time = DateTime.Now;
		    hours.localRotation = Quaternion.Euler(0f, 0f, time.Hour * -hoursToDegrees);
		    minutes.localRotation = Quaternion.Euler(0f, 0f, time.Minute * -minutesToDegrees);
		    seconds.localRotation = Quaternion.Euler(0f, 0f, time.Second * -secondsToDegrees);
        */
        /*
		if(Input.GetKey(KeyCode.UpArrow))
			{
			hours.Rotate(Vector3.forward * -speed);
			//Debug.Log ("Pressed");
			}

		if (Input.GetKey (KeyCode.DownArrow)) 
			{
			hours.Rotate (Vector3.forward * speed);
			//Debug.Log ("Works");
			}


		if(Input.GetKey(KeyCode.RightArrow))
		{
			minutes.Rotate(Vector3.forward * -speed);
			//Debug.Log ("Pressed1");
		}

		if (Input.GetKey (KeyCode.LeftArrow)) 
		{
			minutes.Rotate (Vector3.forward * speed);
			//Debug.Log ("Works1");
		}
        */
       
	}

    public void MovementUp()
    {
        hours.Rotate(Vector3.forward * speed);
    }

    public void MovementDown()
    {
        hours.Rotate(Vector3.forward * -speed);
    }

    public void MovementRight()
    {
        minutes.Rotate(Vector3.forward * speed);
    }

    public void MovementLeft()
    {
        minutes.Rotate(Vector3.forward * -speed);
    }

    public void MovementNone()
    {
        hours.Rotate(Vector3.forward * 0);
        minutes.Rotate(Vector3.forward * 0);
    }
}
	