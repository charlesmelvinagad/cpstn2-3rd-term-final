﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockHandCheck : MonoBehaviour {

	private ClockWinCondition cwc;

	// Use this for initialization
	void Start () {

		cwc = GameObject.Find ("ClockAnimated").GetComponent <ClockWinCondition> ();

	}
	
	// Update is called once per frame
	void Update () {
		
		
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<UnitNumber> ().unitNumber == 1) 
		{
			Debug.Log ("WorksFine");
			cwc.TimeChecks [0] = true;
		}

		else if (other.GetComponent<UnitNumber> ().unitNumber == 2) 
		{
			Debug.Log ("WorksFine1");
			cwc.TimeChecks [1] = true;
		}

		cwc.WinCon ();
	}

	void OnTriggerExit(Collider other)
	{
		if (other.GetComponent<UnitNumber> ().unitNumber == 1) 
		{
			Debug.Log ("ExitsFine");
			cwc.TimeChecks [0] = false;


		}

		else if (other.GetComponent<UnitNumber> ().unitNumber == 2) 
		{
			Debug.Log ("ExitsFine1");
			cwc.TimeChecks [1] = false;
		}

		cwc.WinCon ();
	}
}
