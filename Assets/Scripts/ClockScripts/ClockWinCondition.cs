﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ClockWinCondition : MonoBehaviour {
	
	public bool[] TimeChecks;
	Animator anim;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		



	}

	public void WinCon()
	{
		for (int i = 0; i < TimeChecks.Length; i++) 
		{
			if (TimeChecks.All(c=>c==true)) 
			{
				Debug.Log ("Check!");
				anim.SetBool ("Triggered", true);
			}

		}

	}

}
